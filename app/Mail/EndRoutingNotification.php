<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\IncomingFile;

class EndRoutingNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(IncomingFile $file)
    {
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('backend.pages.email.out_file')
                    ->subject('Outgoing File');
    }
}
