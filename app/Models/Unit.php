<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'category_id',
        'unit_name',
    ];

    public function categories() {
        return $this->belongsTo(Category::class);
    }

    public function users() {
        return $this->belongsToMany('App\Domains\Auth\Models\User')->withTimestamps();
    }
}
