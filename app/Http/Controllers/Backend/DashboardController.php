<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        
        return view('backend.dashboard');
    }

    public function graph_data() {
        $data = [];
        $files = FileCabinet::all()->groupBy(function ($item) {
            return $item->date_dispatched->format('M');
        });
        
        return response()->json($files);
    }
    
}
