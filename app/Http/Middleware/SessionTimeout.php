<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class SessionTimeout
{
    protected $session;
    protected $timeout = 900;

    public function __construct(Store $session) {
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()) {
            if(!$this->session->has('last_login_at'))
                $this->session->put('last_login_at', time());
            elseif(time() - $this->session->get('last_login_at') > $this->getTimeOut()) {
                $this->session->forget('last_login_at');
                \Auth::logout();
                return redirect()->route('frontend.auth.login')->withErrors('You have stayed in active for 15 minutes');
            }
            $this->session->put('last_login_at', time());
        }

        return $next($request);
    }

    protected function getTimeOut() {
        return (env('SESSION_LIFETIME')) ?: $this->timeout;
    }
}
