<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class CategoryTable.
 */
class CategoryTable extends TableComponent
{
    
    
    public $sortField = 'name';

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Category::all()->toQuery();
    }
    
    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'))
                ->searchable()
                ->sortable(),
            Column::make(__('Actions')),
        ];
    }
}
