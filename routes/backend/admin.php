<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\UnitController;
use App\Domains\Auth\Http\Controllers\Backend\User\UserController;
use Tabuna\Breadcrumbs\Trail;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Dashboard'), route('admin.dashboard'));
    });
Route::get('graph_data', [DashboardController::class, 'graph_data'])->name('graph_data');
Route::get('unit_fetch/{id}', [UserController::class, 'unit_fetch'])->name('unit_fetch');

// --------------------------------------- Category Route ---------------------------------//
Route::get('category', [CategoryController::class, 'index'])
    ->name('category.index')
    ->breadcrumbs(function (Trail $trail)  {
        $trail->push(__('Category Management'), route('admin.category.index'));
});
Route::post('category/store', [CategoryController::class, 'store'])
    ->name('category.store')
    ->breadcrumbs(function (Trail $trail)  {
        $trail->push(__('Create Category'), route('admin.category.index'));
});

// ------------------------------------- Unit Route -------------------------------------//
Route::get('unit', [UnitController::class, 'index'])->name('unit.index')->breadcrumbs(function (Trail $trail) {
    $trail->push(__('Unit Management'), route('admin.unit.index'));
});
Route::get('unit/add', [UnitController::class, 'create'])->name('unit.add')->breadcrumbs(function (Trail $trail) {
    $trail->push(__('Add Unit', route('admin.unit.index')));
});
Route::post('unit/store', [UnitController::class, 'store'])->name('unit.store')->breadcrumbs(function (Trail $trail) {
    $trail->push(__('Create Unit'), route('admin.unit.index'));
});