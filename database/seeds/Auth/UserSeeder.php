<?php

use App\Models\Category;
use App\Domains\Auth\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        $category = Category::find(1);

        // Add the master administrator, user id of 1
        User::create([
            'type' => User::TYPE_ADMIN,
            'name' => 'Pura Admin',
            'category' => $category->name,
            'email' => 'admin.pura@test.com',
            'password' => 'secret',
            'email_verified_at' => now(),
            'active' => true,
        ]);
        User::create([
            'type' => User::TYPE_ADMIN,
            'name' => 'Pages Admin',
            'category' => $category->name,
            'email' => 'admin.pages@test.com',
            'password' => 'secret',
            'email_verified_at' => now(),
            'active' => true,
        ]);
        

        if (app()->environment(['local', 'testing'])) {
            User::create([
                'type' => User::TYPE_USER,
                'name' => 'Test User',
                'category' => $category->name,
                'email' => 'user@user.com',
                'password' => 'secret',
                'email_verified_at' => now(),
                'active' => true,
            ]);
        }

        $this->enableForeignKeys();
    }
}
