<?php

use Illuminate\Database\Seeder;
use App\Domains\Auth\Models\User;
use App\Models\Unit;

class UnitUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::find(1)->units()->sync(Unit::find(1)->id);
        User::find(2)->units()->sync(Unit::find(2)->id);
    }
}
