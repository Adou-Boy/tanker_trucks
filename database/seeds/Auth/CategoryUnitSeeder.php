<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Unit;

class CategoryUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pura = Category::find(1);
        Unit::create([
            'category_id' => $pura->id,
            'unit_name' => "FINANCE"
        ]);
        Unit::create([
            'category_id' => $pura->id,
            'unit_name' => "PROCUREMENT"
        ]);
        
        $pages = Category::find(2);
        Unit::create([
            'category_id' => $pages->id,
            'unit_name' => "ADMIN"
        ]);
        Unit::create([
            'category_id' => $pages->id,
            'unit_name' => "FINANCE"
        ]);
        
    }
}
