<div class="modal fade" id="subunitsModal" tabindex="-1" role="dialog" aria-labelledby="subunitsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="subunitsModalLabel">{{$modalTitle}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span ariahidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route($routePage) }}" method="post"  autocomplete="off" class="form-horizontal">
        @csrf
        <div class="modal-body">
          
          <div class="form-group{{ $errors->has('category_name') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('category_name') ? ' is-invalid' : '' }}" name="category_name" id="input-category_name" type="text" placeholder="{{ __('Enter Category Name') }}" required="true" aria-required="true"/>
            @if ($errors->has('category_name'))
              <span id="category_name-error" class="error text-danger" for="input-category_name">{{ $errors->first('category_name') }}</span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('unit_name') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('unit_name') ? ' is-invalid' : '' }}" name="unit_name" id="input-unit_name" type="text" placeholder="{{ __('Enter Unit Name') }}" required="true" aria-required="true"/>
            @if ($errors->has('unit_name'))
              <span id="unit_name-error" class="error text-danger" for="input-unit_name">{{ $errors->first('unit_name') }}</span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('sub_unit_name') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('sub_unit_name') ? ' is-invalid' : '' }}" name="sub_unit_name" id="input-sub_unit_name" type="text" placeholder="{{ __('Enter Sub-unit Name') }}" required="true" aria-required="true"/>
            @if ($errors->has('sub_unit_name'))
              <span id="sub_unit_name-error" class="error text-danger" for="input-sub_unit_name">{{ $errors->first('sub_unit_name') }}</span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('id') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" id="input-id" type="number"  hidden/>
            @if ($errors->has('id'))
              <span id="id-error" class="error text-danger" for="input-id">{{ $errors->first('id') }}</span>
            @endif
          </div>

          <button type="submit" class="btn btn-success">Confirm</button>
          <button type="button" class="btn btn-danger float-right" data-dismiss="modal">Cancel</button>
          
        </div>
      </form>
    </div>
  </div>
</div>