<div class="modal fade" id="configurationsModal" tabindex="-1" role="dialog" aria-labelledby="configurationsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="configurationsModalLabel">{{$modalTitle}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span ariahidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route($routePage) }}" method="post"  autocomplete="off" class="form-horizontal">
        @csrf
        <div class="modal-body">
          
          <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __($placeholder_value) }}" required="true" aria-required="true"/>
            @if ($errors->has('name'))
              <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('id') ? ' has-danger' : '' }}">
            <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" id="input-id" type="number"  hidden/>
            @if ($errors->has('id'))
              <span id="id-error" class="error text-danger" for="input-id">{{ $errors->first('id') }}</span>
            @endif
          </div>

          <button type="submit" class="btn btn-success">Confirm</button>
          <button type="button" class="btn btn-danger float-right" data-dismiss="modal">Close</button>
          
        </div>
      </form>
    </div>
  </div>
</div>