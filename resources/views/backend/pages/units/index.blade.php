@extends('backend.layouts.app')

@section('title', __('Unit Management'))

@section('content')
<x-backend.card>
    <x-slot name="body">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Unit Management</h4>
            <p class="card-category tex-muted"> List of currently available units</p>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12 text-right">
                  <a href="#" class="btn btn-sm btn-info" onclick="newUnitModal()">
                  <i class="c-icon cil-plus"></i>
                  new unit
                  </a>
              </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="my-table">
                        <thead>
                            <tr class="bg-secondary">
                                <th>Category</th>
                                <th>Unit</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        @foreach ($units as $unit)
                        @foreach($unit['units'] as $unit_category)
                        @if(isset($unit_category))
                            <tr>
                                <td>{{$unit->name }}</td>
                                <td>{{ $unit_category->unit_name }}</td>
                                
                                <td class="text-center">
                                    <a href="#" class="btn-sm btn btn-primary" title="Manage">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="#" class="btn-sm btn btn-warning" title="Manage">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <!-- <a href="#" class="btn-sm btn btn-danger" data-method="delete" 
                                    onclick="return confirm('Do you want to delete Category?')" 
                                    title="Delete Category">
                                        <i class="fas fa-trash"></i>
                                    </a> -->
                                </td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
          </div>
        </div>
        @include('backend.includes.form_partials.subs', ['modalTitle' => 'Unit Management', 'placeholder_value' => 'Enter Unit Name', 'routePage' => 'admin.unit.store'])
    </x-slot>
</x-backend.card>
@endsection
@push('before-scripts')
<script>
    function newUnitModal() {
        $('form').find('input[type=text]').val('');
        $('#unitsModal').modal('show');
    }
    function editModal(myId, myValue) {
        $('form').find('input[type=text]').val(myValue);
        $('form').find('input[type=number]').val(myId);
        $('#subsectionsModal').modal('show');
    }
</script>
@endpush
