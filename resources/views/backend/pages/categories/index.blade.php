@extends('backend.layouts.app')

@section('title', __('Category Management'))

@section('content')
<x-backend.card>
    <x-slot name="body">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">Category Management</h4>
            <p class="card-category text-muted"> List of currently available categories</p>
          </div>
          <div class="card-body">
            @include('backend.includes.form_partials.new_config', ['newconfiguration' => 'new category'])
            <div class="row mt-2">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="my-table">
                        <thead>
                            <tr class="bg -secondary">
                                <th>Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                            <tr>
                                <td>{{$category['name']}}</td>
                                <td class="text-center">
                                    <a href="#" class="btn-sm btn btn-primary" title="Manage">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="#" class="btn-sm btn btn-warning" title="Manage">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <!-- <a href="#" class="btn-sm btn btn-danger" data-method="delete" 
                                    onclick="return confirm('Do you want to delete Category?')" 
                                    title="Delete Category">
                                        <i class="fas fa-trash"></i>
                                    </a> -->
                                </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
          </div>
        </div>
        @include('backend.includes.form_partials.config_form', ['modalTitle' => 'Category Management', 'placeholder_value' => 'Enter Category Name', 'routePage' => 'admin.category.store'])
    </x-slot>
</x-backend.card>
@endsection