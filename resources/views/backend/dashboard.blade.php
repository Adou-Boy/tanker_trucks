@extends('backend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
<x-backend.card>
    <x-slot name="body">
    <div class="row">
        <div class="col-sm-6 col-lg-6">
            <div class="card text-white bg-gradient-secondary text-info">
                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start"><div>
                <div class="text-value-lg">0</div>
                <div>Total Vehicles</div>
            </div>
            <h1 class="mt-2 mx-2" style="height:50px;"><icon class="cil-library"></icon></h1>
        </div>
        </div>
        </div>
        <div class="col-sm-3 col-lg-3">
            <div class="card text-white bg-gradient-secondary text-info">
                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start"><div>
                <div class="text-value-lg">0</div>
                <div>Tested Vehicles</div>
            </div>
            <h1 class="mt-2 mx-2" style="height:50px;"><icon class="cil-arrow-thick-to-right"></icon></h1>
        </div>
        </div>
        </div>
        <div class="col-sm-3 col-lg-3">
            <div class="card text-white bg-gradient-secondary text-info">
                <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start"><div>
                <div class="text-value-lg">0</div>
                <div>Non-tested Vehicles</div>
            </div>
            <h1 class="mt-2 mx-2" style="height:50px;"><icon class="cil-arrow-thick-to-left"></icon></h1>
        </div>
        </div>
        </div>
        
    </div>
    
    
    <!-- START -->
    <div class="card mt-5">
        
    </div>
    </x-slot>
    
</x-backend.card>
@endsection